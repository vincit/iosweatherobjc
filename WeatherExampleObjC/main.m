//
//  main.m
//  WeatherExampleObjC
//
//  Created by Juha Riippi on 12/11/15.
//  Copyright © 2015 Vincit Oy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
